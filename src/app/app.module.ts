import { SharedModule } from './shared.module';

//angular dependencies
import { ErrorHandler, NgModule } from '@angular/core';

//ionic dependencies
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
//main App
import { MyApp } from './app.component';

// ionic components
import { MODULES, PROVIDERS, NATIVES } from './app.imports';

// priveder 
import { Geolocation } from '@ionic-native/geolocation';
import {
  NativeGeocoder
} from '@ionic-native/native-geocoder';
@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    MODULES,
    IonicModule.forRoot(MyApp),
    SharedModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    PROVIDERS,
    NATIVES,
    Geolocation,
    NativeGeocoder,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
