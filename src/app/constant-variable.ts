export const ConstantVariable: any = {
	//App Enviroment Settings
	APP_NAME					: "OLOJI",
	APP_HOST					: "www.ologi.in",
	APP_EMAIL					: "oloji@gmail.com",
	APP_CONTACT				: "+919988776655",
	APP_ADDRESS				: "+xxxxxxxxxxxxxxxxx",

	//URL API
	APIURL						: "http://localhost/oloji/api/",
	
	//firebaseConfig
	apiKey						: "xxxxxxxxxxxxxxxxx",
	authDomain				: "xxxxxxxxxxxxxxxxx",
	databaseURL				: "xxxxxxxxxxxxxxxxx",
	projectId					: "xxxxxxxxxxxxxxxxx",
	storageBucket			: "xxxxxxxxxxxxxxxxx",
	messagingSenderId	: "xxxxxxxxxxxxxxxxx",
};