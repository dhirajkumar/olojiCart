import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-view-cart',
	templateUrl: 'view-cart.html',
})
export class ViewCartPage {

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		public modalCtrl: ModalController) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ViewCartPage');
	}
	dismiss() {
		this.viewCtrl.dismiss();
	}
	login() {
		let checkOut = this.modalCtrl.create("LoginPage");
		checkOut.onDidDismiss(data => {
			console.log(data);
		});
		checkOut.present();
	}
}
