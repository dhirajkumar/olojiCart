import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, 
	     NativeGeocoderReverseResult} from '@ionic-native/native-geocoder';
/**
 * Generated class for the BillinginfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-billinginfo',
  templateUrl: 'billinginfo.html',
})
export class BillinginfoPage {

  constructor(public navCtrl: NavController, 
  	          public navParams: NavParams,
	          private geolocation: Geolocation,
	          private nativeGeocoder: NativeGeocoder) {
	  
	
  }

  ionViewDidLoad() {
	  this.getUserLocation();
    console.log('ionViewDidLoad BillinginfoPage');
	 
  }


  getUserLocation(){
	  console.log("get user location");
	  this.geolocation.getCurrentPosition().then((resp) => {
		  // resp.coords.latitude
		  // resp.coords.longitude
		  console.log("user cords " + resp.coords.latitude);
		  console.log("user cords " + resp.coords.longitude);
		 // this.initMap(resp.coords.latitude, resp.coords.latitude)
	  }).catch((error) => {
		  console.log('Error getting location', JSON.stringify(error));
	  });
  }

	initMap(x, y) {

		this.nativeGeocoder.reverseGeocode(x, y)
			.then((result: NativeGeocoderReverseResult) => console.log("location"+JSON.stringify(result)))
			.catch((error: any) => console.log(error));
	}

}
