import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-check-out',
  templateUrl: 'check-out.html',
})
export class CheckOutPage {

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams
  	) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckOutPage');
	}
 }
