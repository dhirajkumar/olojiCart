import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
 items:any;
 categories:any;
 constructor(
 	public navCtrl: NavController,
	public modalCtrl:ModalController) {
	 this.items =
[
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010030.jpg",
		name: "Goldiee Corinder Powder",
		price: 205,
		unit: "kg",
		discount: "10%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000420.jpg",
		name: "Everest Kasuri Meethi",
		price: 975,
		unit: "kg",
		discount: "5%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000427.jpg",
		name: "Everest Sabji Masala",
		price: 530,
		unit: "kg",
		discount: "9%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010202.jpg",
		name: "Sundrop Heart Oil",
		price: 225,
		unit: "litre",
		discount: "16%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010204.jpg",
		name: "Sundrop Superlite Advance Oil",
		price: 185,
		unit: "litre",
		discount: "14%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000419.jpg",
		name: "Everest Cumin Powder",
		price: 584,
		unit: "kg",
		discount: "10%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000420.jpg",
		name: "Everest Kasuri Meethi",
		price: 975,
		unit: "kg",
		discount: "5%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000427.jpg",
		name: "Everest Sabji Masala",
		price: 530,
		unit: "kg",
		discount: "9%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000420.jpg",
		name: "Everest Kasuri Meethi",
		price: 975,
		unit: "kg",
		discount: "5%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000427.jpg",
		name: "Everest Sabji Masala",
		price: 530,
		unit: "kg",
		discount: "9%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010202.jpg",
		name: "Sundrop Heart Oil",
		price: 225,
		unit: "litre",
		discount: "16%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010204.jpg",
		name: "Sundrop Superlite Advance Oil",
		price: 185,
		unit: "litre",
		discount: "14%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000419.jpg",
		name: "Everest Cumin Powder",
		price: 584,
		unit: "kg",
		discount: "10%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000420.jpg",
		name: "Everest Kasuri Meethi",
		price: 975,
		unit: "kg",
		discount: "5%"
	},
	{
		src: "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000427.jpg",
		name: "Everest Sabji Masala",
		price: 530,
		unit: "kg",
		discount: "9%"
	}
];
	 this.categories = [
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_573,h_429,q_auto/General/website--store-banners-01.jpg",
		 	name:"Baby Store"
	 	 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-10.jpg",
		 	name:"Healthy Drink Store" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-12.jpg",
		 	name:"Frozen Store" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-15.jpg",
		 	name:"Fruits and Vegetables" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-final-14.jpg",
		 	name:"Pulse / Dal Store" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-final-15.jpg",
		 	name:"Spice Store" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-final-16.jpg",
		 	name:"Kitchen Essential Store" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-final-17.jpg",
		 	name:"RIce Store" 
		 },
		 { 
		 	src: "https://res.cloudinary.com/ziobasket/image/upload/w_275,h_205,q_auto/General/website--store-banners-final-18.jpg",
		 	name:"Dryfruits Store" 
		 }
	 ];

	}
   public showProductDetails() {
	   let productDetails = this.modalCtrl.create("ShowPage");
	   productDetails.onDidDismiss(data => {
		   console.log(data);
	   });
	   productDetails.present();
	}
	viewCart() {
		let checkout = this.modalCtrl.create("ViewCartPage");
		checkout.onDidDismiss(data => {
			console.log(data);
		});
		checkout.present();
	}
} 