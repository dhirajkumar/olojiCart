import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-show',
  templateUrl: 'show.html',
})
export class ShowPage {
	buttonClass: any = "B3";
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  changePacket(btn) {
	  // console.log(btn);
	  this.buttonClass = "B1";
  }
}
