import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import * as _ from 'lodash';

@IonicPage()
@Component({
	selector: 'page-category',
	templateUrl: 'category.html',
})
export class CategoryPage {
	search: any;
	items: any;
	searchItems: any; 
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
    public modelCtrl: ModalController) {
		this.search = {
			query: ''
		};
		this.items = JSON.parse(
			`[
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010030.jpg",
    "name": "Goldiee Corinder Powder",
    "price": 205,
    "unit": "kg",
    "discount": "10%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010031.jpg",
    "name": "Goldiee Turmeric Powder",
    "price": 235,
    "unit": "kg",
    "discount": "11%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010033.jpg",
    "name": "Goldiee Meat Masala",
    "price": 750,
    "unit": "Kg",
    "discount": "14%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010038.jpg",
    "name": "Goldiee Kashmiri Mirch",
    "price": 500,
    "unit": "kg",
    "discount": "12%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010041.jpg",
    "name": "Goldee Chicken Masala",
    "price": 500,
    "unit": "kg",
    "discount": "9%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010044.jpg",
    "name": "Goldiee Garam Masala",
    "price": 300,
    "unit": "kg",
    "discount": "18%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010046.jpg",
    "name": "Goldiee Sabji Masala",
    "price": 450,
    "unit": "kg",
    "discount": "16%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010048.jpg",
    "name": "Goldiee Chat Masala",
    "price": 475,
    "unit": "kg",
    "discount": "11%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010202.jpg",
    "name": "Sundrop Heart Oil",
    "price": 225,
    "unit": "litre",
    "discount": "16%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000010204.jpg",
    "name": "Sundrop Superlite Advance Oil",
    "price": 185,
    "unit": "litre",
    "discount": "14%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000419.jpg",
    "name": "Everest Cumin Powder",
    "price": 584,
    "unit": "kg",
    "discount": "10%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000420.jpg",
    "name": "Everest Kasuri Meethi",
    "price": 975,
    "unit": "kg",
    "discount": "5%"
  },
  {
    "src": "https://res.cloudinary.com/ziobasket/image/upload/w_200,h_200,q_auto/ProductImages/SKU000000427.jpg",
    "name": "Everest Sabji Masala",
    "price": 530,
    "unit": "kg",
    "discount": "9%"
  }
]
`);
		
		this.searchBox();
	}

	ionViewDidLoad() {
		this.searchBox();
		console.log('ionViewDidLoad CategoryPage');
		console.log(this.items);
	}
	searchBox() {
		var tosearch = this.search.query;
		// var tosearch="/"+this.search.query+"/i";
		// console.log(new RegExp("acc").ignoreCase);
		tosearch = tosearch.trim();
		this.searchItems = _.filter(this.items, function(obj) {
			return obj["name"].toLowerCase().indexOf(tosearch.toLowerCase()) > -1
		});
	}
  public showProductDetails() {
    let productDetails = this.modelCtrl.create("ShowPage");
    productDetails.onDidDismiss(data => {
      console.log(data);
    });
    productDetails.present();
  }
}
