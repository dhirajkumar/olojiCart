import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, App, LoadingController, Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  pages: any = "";
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App
  ) {
    this.pages = {
      no: 0,
      class: ""
    };
  }

  // Slider methods
  @ViewChild('slider') slider: Slides;
  @ViewChild('innerSlider') innerSlider: Slides;
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
	dismiss() {
		this.viewCtrl.dismiss();
	}
  goToLogin() {
    // this.slider.slideTo(0);
    this.pages ={
      no: 0,
      class: "animated-fast slideInDown"
    };
  }

  goToSignup() {
    // this.slider.slideTo(1);
    this.pages = {
      no: 2,
      class: "animated-fast slideInUp"
    };
  }

  slideNext() {
    // this.innerSlider.slideNext();
    this.pages = {
      no: 1,
      class: "animated-fast slideInRight"
    };
  }

  slidePrevious() {
    // this.innerSlider.slidePrev();
    this.pages = {
      no: 0,
      class: "animated-fast slideInLeft"
    };
  }

  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  login() {
    this.presentLoading('Thanks for signing up!');
    // this.navCtrl.push(HomePage);
  }
  billinginfo(){
    this.navCtrl.push('BillinginfoPage');
  }
}
